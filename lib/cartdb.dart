import 'package:hive/hive.dart';
part 'cartdb.g.dart';

@HiveType(typeId: 1) // this is like generating a table
class Cart extends HiveObject {
  @HiveField(0)
  String image;
  @HiveField(1)
  String title;
  @HiveField(2)
  String price;
  @HiveField(3)
  int quantity = 1;
  Cart(this.image, this.title, this.price,this.quantity);
}

