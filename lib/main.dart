import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:hive/hive.dart';
import 'package:m7l/cartdb.dart';
import 'package:m7l/models/CategoriesModel.dart';
import 'package:m7l/models/ProductList.dart';
import 'package:m7l/screens/categoriesmenu.dart';
import 'package:m7l/screens/categoriespage.dart';
import 'package:m7l/screens/homepage.dart';
import 'package:m7l/screens/mycartpage.dart';
import 'package:m7l/screens/profilepage.dart';
import 'package:m7l/screens/shopspage.dart';
import 'package:m7l/userdb.dart';
import 'package:path_provider/path_provider.dart' as path_provider;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final appDocumentDirectory = await path_provider
      .getApplicationDocumentsDirectory(); // providing a path to store the database
  Hive.init(appDocumentDirectory.path);
  Hive.registerAdapter(UserAdapter());
  var box = await Hive.openBox<User>('AddressBox');
   Hive.registerAdapter(CartAdapter());
  var cartbox = await Hive.openBox<Cart>('CartBox');
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
          appBarTheme: AppBarTheme(
        color: Color(0xFFFFFFFF),
      )),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _selectedIndex = 0;
  int _currentIndex = 0;
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      body: getselectedpage(),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Image.asset('images/Home01.png', height: 30),
            activeIcon: Image.asset('images/Home02.png', height: 30),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.search),
            label: 'Categories',
          ),
          BottomNavigationBarItem(
            icon: Image.asset('images/Shop01.png', height: 30),
            activeIcon: Image.asset('images/Shop02.png', height: 30),
            label: 'Shops',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.add_shopping_cart),
            label: 'My Cart',
          ),
          BottomNavigationBarItem(
            icon: Image.asset('images/Profile01.png', height: 30),
            activeIcon: Image.asset('images/Profile02.png', height: 30),
            label: 'Profile',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.black,
        onTap: _onItemTapped,
      ),
    );
  }

  getselectedpage() {
    switch (_selectedIndex) {
      case 0:
        return HomePage();
        break;
      case 1:
        return CategoriesMenuPage();
        break;
      case 2:
        return ShopsPage();
        break;
      case 3:
        return MyCartPage();
        break;
      case 4:
        return ProfilePage();
        break;
    }
  }
}
