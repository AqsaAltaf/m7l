class CartListModel {
  String image;
  String title;
  String price;
  int quantity;
  CartListModel({this.image, this.title, this.price,this.quantity});
}

/*List<CartListModel> getCartList() {
  return [
    CartListModel(
        image: 'images/macbook.png', title: 'iMAC Laptop', price: '8950'),
    CartListModel(
        image: 'images/macbook.png', title: 'iMAC Laptop', price: '8950'),
  ];
}*/
