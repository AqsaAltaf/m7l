import 'package:flutter/material.dart';

class CategoriesImagesListModel {
  String image;
  //List<String>
  String title;
  String price;
  String material;
  List<Color> color;
  List<String> size;
  String description;
  String stylecollection;
  CategoriesImagesListModel(
      {this.image,
      this.title,
      this.price,
      this.material,
      this.color,
      this.size,
      this.description,
      this.stylecollection});
}

List<CategoriesImagesListModel> getCategoriesImagesList() {
  return [
    CategoriesImagesListModel(
        image: 'images/macbook.png',
        title: 'iMAC Laptop',
        description: '13 inch, Intel Core i3',
        price: '8950',
        material: 'Leather',
        stylecollection: 'Korean Sweet Metal',
        color: [
          Colors.redAccent[100], Colors.grey[600], Colors.blue,
        ],
         size: [
         'Small', 'Medium', 'Large',
        ],
        ),
    CategoriesImagesListModel(
        image: 'images/macbook.png',
        title: 'iMAC Laptop',
        description: '13 inch, Intel Core i3',
        price: '8950',
        material: 'Leather',
        stylecollection: 'Korean Sweet Metal',
        color: [
          Colors.yellow, Colors.red, Colors.blue
        ],
        size: [
         'Small', 'Medium', 'Large',
        ]),
    CategoriesImagesListModel(
        image: 'images/macbook.png',
        title: 'iMAC Laptop',
        description: '13 inch, Intel Core i3',
        price: '8950',
        material: 'Leather',
        stylecollection: 'Korean Sweet Metal',
        color: [
          Colors.redAccent[100], Colors.grey[600], Colors.blue
        ],
        size: [
         'Small', 'Medium', 'Large',
        ]),
    CategoriesImagesListModel(
        image: 'images/macbook.png',
        title: 'iMAC Laptop',
        description: '13 inch, Intel Core i3',
        price: '8950',
        material: 'Leather',
        stylecollection: 'Korean Sweet Metal',
        color: [
          Colors.redAccent[100], Colors.grey[600], Colors.blue
        ],
        size: [
         'Small', 'Medium', 'Large',
        ]),
    CategoriesImagesListModel(
        image: 'images/macbook.png',
        title: 'iMAC Laptop',
        description: '13 inch, Intel Core i3',
        price: '8950',
        material: 'Leather',
        stylecollection: '',
        color: [
          Colors.redAccent[100], Colors.grey[600], Colors.blue
        ],
        size: [
         'Small', 'Medium', 'Large',
        ]
        ),
    CategoriesImagesListModel(
        image: 'images/macbook.png',
        title: 'iMACC Laptop',
        description: '13 inch, Intel Core i3',
        price: '8950',
        material: '',
        stylecollection: '',
        color: [
          Colors.redAccent[100], Colors.grey[600], Colors.blue
        ],
        size: [
         'Small', 'Medium', 'Large',
        ]
        ),
  ];
}

