class CategoriesListModel {
  String name;
  CategoriesListModel(this.name);

  static List<CategoriesListModel> getCategoriesList() {
    return [
      CategoriesListModel('New Arrivals'),
      CategoriesListModel('Low to High'),
      CategoriesListModel('High to Low'),
      CategoriesListModel('Sales'),
    ];
  }
}
