class CategoriesModel {
  String image;
  String name;
  CategoriesModel(this.image, this.name);

  static List<CategoriesModel> getCategories() {
    return [
      CategoriesModel('images/iphone-12.png', 'Electronics'),
      CategoriesModel('images/iphone-12.png', 'Mobiles'),
      CategoriesModel('images/iphone-12.png', 'Perfumes'),
      CategoriesModel('images/iphone-12.png', 'Watches'),
      CategoriesModel('images/iphone-12.png', 'Electronics'),
    ];
  }
}
