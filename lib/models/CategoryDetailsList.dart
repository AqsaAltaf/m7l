class CategoryDetailsList {
  String name;
  List<Map<String, dynamic>> categorydetail;
  CategoryDetailsList({this.name, this.categorydetail});
}

List<CategoryDetailsList> Categories = [
  CategoryDetailsList(name: "Electronics", categorydetail: [
    {'image': 'images/macbook.png', 'title': 'Laptops', 'price': '1990'},
    {'image': 'images/macbook.png', 'title': 'Laptops', 'price': '1990'},
    {'image': 'images/macbook.png', 'title': 'Laptops', 'price': '1990'},
     {'image': 'images/macbook.png', 'title': 'Laptops', 'price': '1990'},
    {'image': 'images/macbook.png', 'title': 'Laptops', 'price': '1990'},
    {'image': 'images/macbook.png', 'title': 'Laptops', 'price': '1990'},
     {'image': 'images/macbook.png', 'title': 'Laptops', 'price': '1990'},
    {'image': 'images/macbook.png', 'title': 'Laptops', 'price': '1990'},
    
  ]),
  CategoryDetailsList(name: "Mobiles", categorydetail: [
    {'image': 'images/watch.png', 'title': 'Silver Watch', 'price': '1990'},
    {'image': 'images/watch.png', 'title': 'Silver Watch', 'price': '1990'},
    {'image': 'images/watch.png', 'title': 'Silver Watch', 'price': '1990'},
  ]),
  CategoryDetailsList(name: "Toys", categorydetail: [
    {'image': 'images/watch.png', 'title': 'Silver Watch', 'price': '1990'},
    {'image': 'images/watch.png', 'title': 'Silver Watch', 'price': '1990'},
    {'image': 'images/watch.png', 'title': 'Silver Watch', 'price': '1990'},
  ]),
  CategoryDetailsList(name: "Household", categorydetail: [
    {'image': 'images/watch.png', 'title': 'Silver Watch', 'price': '1990'},
    {'image': 'images/watch.png', 'title': 'Silver Watch', 'price': '1990'},
    {'image': 'images/watch.png', 'title': 'Silver Watch', 'price': '1990'},
  ]),
  CategoryDetailsList(name: "Watches", categorydetail: [
    {'image': 'images/watch.png', 'title': 'Silver Watch', 'price': '1990'},
    {'image': 'images/watch.png', 'title': 'Silver Watch', 'price': '1990'},
    {'image': 'images/watch.png', 'title': 'Silver Watch', 'price': '1990'},
  ]),
  CategoryDetailsList(name: "Perfumes", categorydetail: [
    {'image': 'images/watch.png', 'title': 'Silver Watch', 'price': '1990'},
    {'image': 'images/watch.png', 'title': 'Silver Watch', 'price': '1990'},
    {'image': 'images/watch.png', 'title': 'Silver Watch', 'price': '1990'},
  ]),
  CategoryDetailsList(name: "Fashion", categorydetail: [
    {'image': 'images/watch.png', 'title': 'Silver Watch', 'price': '1990'},
    {'image': 'images/watch.png', 'title': 'Silver Watch', 'price': '1990'},
    {'image': 'images/watch.png', 'title': 'Silver Watch', 'price': '1990'},
  ]),
  CategoryDetailsList(name: "Beauty", categorydetail: [
    {'image': 'images/watch.png', 'title': 'Silver Watch', 'price': '1990'},
    {'image': 'images/watch.png', 'title': 'Silver Watch', 'price': '1990'},
    {'image': 'images/watch.png', 'title': 'Silver Watch', 'price': '1990'},
  ]),
  CategoryDetailsList(name: "Sports and Fitness", categorydetail: [
    {'image': 'images/watch.png', 'title': 'Silver Watch', 'price': '1990'},
    {'image': 'images/watch.png', 'title': 'Silver Watch', 'price': '1990'},
    {'image': 'images/watch.png', 'title': 'Silver Watch', 'price': '1990'},
  ]),
  CategoryDetailsList(name: "Sneakers", categorydetail: [
    {'image': 'images/watch.png', 'title': 'Silver Watch', 'price': '1990'},
    {'image': 'images/watch.png', 'title': 'Silver Watch', 'price': '1990'},
    {'image': 'images/watch.png', 'title': 'Silver Watch', 'price': '1990'},
  ]),
  CategoryDetailsList(name: "Kitchen", categorydetail: [
    {'image': 'images/watch.png', 'title': 'Silver Watch', 'price': '1990'},
    {'image': 'images/watch.png', 'title': 'Silver Watch', 'price': '1990'},
    {'image': 'images/watch.png', 'title': 'Silver Watch', 'price': '1990'},
  ]),
  CategoryDetailsList(name: "Food", categorydetail: [
    {'image': 'images/watch.png', 'title': 'Silver Watch', 'price': '1990'},
    {'image': 'images/watch.png', 'title': 'Silver Watch', 'price': '1990'},
    {'image': 'images/watch.png', 'title': 'Silver Watch', 'price': '1990'},
  ]),
];
