List<Map<String, dynamic>> Address = [
  {
    "Country": "United Arab Emirates",
    "City": "Abu Dhabi",
    "Address": "Al Reem Island, Blue Tower",
    "ContactNumber": "050 xxx xxxx"
  }
];
// this info is just put here so the first one doesnt return empty

class AddressDetailModel {
  String country;
  String city;
  String address;
  String contactnumber;
  AddressDetailModel({this.country, this.city, this.address, this.contactnumber});
}

