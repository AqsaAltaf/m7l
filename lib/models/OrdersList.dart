class OrdersListModel {
  String ordernumber;
  String date;
  String image;
  String price;
  String status;
  List<OrderDetailModel> orderdetail;
  OrdersListModel(
      {this.ordernumber,
      this.date,
      this.image,
      this.price,
      this.status,
      this.orderdetail});
}

class OrderDetailModel {
  String image;
  String title;
  String price;
  String quantity;
  OrderDetailModel({this.image, this.title, this.price, this.quantity});
}

List<OrdersListModel> getOrdersList() {
  return [
    OrdersListModel(
        ordernumber: '#1225201',
        date: '29/09/2020',
        image: 'images/macbook.png',
        price: '1710',
        status: 'Waiting for Confirmation',
        orderdetail: [
          OrderDetailModel(
              image: 'images/earrings.jpg',
              title: 'Earing with diamond',
              price: '860',
              quantity: '1'),
        ]),
    OrdersListModel(
        ordernumber: '#1225199',
        date: '17/08/2020',
        image: 'images/macbook.png',
        price: '650',
        status: 'Delivered',
        orderdetail: [
          OrderDetailModel(
              image: 'images/earrings.jpg',
              title: 'Earing with diamond',
              price: '860',
              quantity: '1'),
        ]),
  ];
}
