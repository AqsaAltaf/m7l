class OtherShopList {
  List<OtherShopDetailModel> othershopdetail;
  List<OtherShopNameModel> othershopname;
  OtherShopList({this.othershopdetail, this.othershopname});
}

class OtherShopDetailModel {
  String image;
  OtherShopDetailModel({this.image});
}

class OtherShopNameModel {
  String title;
  OtherShopNameModel({this.title});
}

List<OtherShopList> getOtherShopDetails() {
  return [
    OtherShopList(
      othershopdetail: [
        OtherShopDetailModel(image: 'images/ecomarket.jpg'),
        OtherShopDetailModel(image: 'images/ecomarket.jpg'),
        OtherShopDetailModel(image: 'images/ecomarket.jpg'),
         OtherShopDetailModel(image: 'images/ecomarket.jpg'),
        OtherShopDetailModel(image: 'images/ecomarket.jpg'),
        OtherShopDetailModel(image: 'images/ecomarket.jpg'),
         OtherShopDetailModel(image: 'images/ecomarket.jpg'),
        OtherShopDetailModel(image: 'images/ecomarket.jpg'),
        OtherShopDetailModel(image: 'images/ecomarket.jpg'),
      ],
      othershopname: [
        OtherShopNameModel(title: 'Home made bakery'),
        OtherShopNameModel(title: 'Market shop'),
        OtherShopNameModel(title: 'Eco Market'),
        OtherShopNameModel(title: 'Home made bakery'),
        OtherShopNameModel(title: 'Market shop'),
        OtherShopNameModel(title: 'Eco Market'),
        OtherShopNameModel(title: 'Home made bakery'),
        OtherShopNameModel(title: 'Market shop'),
        OtherShopNameModel(title: 'Eco Market'),
      ],
    ),
  ];
}
