class ProductList {
  String name;
  List<ProductDetailModel> productdetail;
  ProductList({this.name, this.productdetail});
}

class ProductDetailModel {
  String image;
  String title;
  String price;
  ProductDetailModel({this.image, this.title, this.price});
}

List<ProductList> getDetails() {
  return [
    ProductList(name: "Special Products", productdetail: [
      ProductDetailModel(
          image: 'images/watch.png', title: 'Silver Watch', price: '1990'),
      ProductDetailModel(
          image: 'images/watch.png', title: 'Silver Watch', price: '1990'),
      ProductDetailModel(
          image: 'images/watch.png', title: 'Silver Watch', price: '1990'),
    ]),
    ProductList(name: "Trending Deals", productdetail: [
      ProductDetailModel(
          image: 'images/watch.png', title: 'Silver Watch', price: '1990'),
      ProductDetailModel(
          image: 'images/watch.png', title: 'Silver Watch', price: '1990'),
      ProductDetailModel(
          image: 'images/watch.png', title: 'Silver Watch', price: '1990'),
    ]),
  ];
}
