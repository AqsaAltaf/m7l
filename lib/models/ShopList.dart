class ShopList {
  List<ShopDetailModel> shopdetail;
  List<ShopNameModel> shopname;
  ShopList({this.shopdetail, this.shopname});
}

class ShopDetailModel {
  String image;
  ShopDetailModel({this.image});
}

class ShopNameModel {
  String title;
  ShopNameModel({this.title});
}

List<ShopList> getShopDetails() {
  return [
    ShopList(
      shopdetail: [
        ShopDetailModel(image: 'images/ecomarket.jpg'),
        ShopDetailModel(image: 'images/ecomarket.jpg'),
        ShopDetailModel(image: 'images/ecomarket.jpg'),
          ShopDetailModel(image: 'images/ecomarket.jpg'),
        ShopDetailModel(image: 'images/ecomarket.jpg'),
        ShopDetailModel(image: 'images/ecomarket.jpg'),
      ],
      shopname: [
        ShopNameModel(title: 'Home made bakery'),
        ShopNameModel(title: 'Market shop'),
        ShopNameModel(title: 'Eco Market'),
        ShopNameModel(title: 'Home made bakery'),
        ShopNameModel(title: 'Market shop'),
        ShopNameModel(title: 'Eco Market'),
      ],
    ),
  ];
}
