class VerifiedShopList {
  String name;
  List<VerifiedShopDetailModel> verifiedshopdetail;
  List<VerifiedShopNameModel> verifiedshopname;
  VerifiedShopList({this.name, this.verifiedshopdetail, this.verifiedshopname});
}

class VerifiedShopDetailModel {
  String image;
  VerifiedShopDetailModel({this.image});
}

class VerifiedShopNameModel {
  String title;
  VerifiedShopNameModel({this.title});
}

List<VerifiedShopList> getVerfiedShopDetails() {
  return [
    VerifiedShopList(
      verifiedshopdetail: [
        VerifiedShopDetailModel(image: 'images/ecomarket.jpg'),
        VerifiedShopDetailModel(image: 'images/ecomarket.jpg'),
        VerifiedShopDetailModel(image: 'images/ecomarket.jpg'),
        VerifiedShopDetailModel(image: 'images/ecomarket.jpg'),
        VerifiedShopDetailModel(image: 'images/ecomarket.jpg'),
        VerifiedShopDetailModel(image: 'images/ecomarket.jpg'),
        VerifiedShopDetailModel(image: 'images/ecomarket.jpg'),
        VerifiedShopDetailModel(image: 'images/ecomarket.jpg'),
        VerifiedShopDetailModel(image: 'images/ecomarket.jpg'),
        VerifiedShopDetailModel(image: 'images/ecomarket.jpg'),
      ],
      verifiedshopname: [
        VerifiedShopNameModel(title: 'Home made bakery'),
        VerifiedShopNameModel(title: 'Market shop'),
        VerifiedShopNameModel(title: 'Eco Market'),
        VerifiedShopNameModel(title: 'Home made bakery'),
        VerifiedShopNameModel(title: 'Market shop'),
        VerifiedShopNameModel(title: 'Eco Market'),
        VerifiedShopNameModel(title: 'Market shop'),
        VerifiedShopNameModel(title: 'Eco Market'),
        VerifiedShopNameModel(title: 'Eco Market'),
        VerifiedShopNameModel(title: 'Home made bakery'),
      ],
    ),
  ];
}
