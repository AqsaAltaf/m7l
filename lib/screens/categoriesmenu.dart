import 'package:flutter/material.dart';
import 'package:m7l/main.dart';
import 'package:m7l/models/CategoryDetailsList.dart';
import 'package:m7l/screens/categoriespage.dart';
import 'package:m7l/screens/mycartpage.dart';
import 'package:m7l/screens/shopspage.dart';

import 'profilepage.dart';

class CategoriesMenuPage extends StatefulWidget {
  @override
  _CategoriesMenuPageState createState() => _CategoriesMenuPageState();
}

class _CategoriesMenuPageState extends State<CategoriesMenuPage> {
  int _hasbeenPressed = 0;
  int _selectedIndex = 0;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        leading: Padding(
          padding: const EdgeInsets.only(left: 15.0),
          child: Image.asset('images/APP_ICON_01.png'),
        ),
        automaticallyImplyLeading: false,
        titleSpacing: 0.1,
        leadingWidth: 70,
        title: Container(
          height: 50,
          width: 330,
          margin: EdgeInsets.only(left: 10, bottom: 1, right: 10),
          padding: EdgeInsets.all(10.0),
          child: TextField(
            cursorColor: Colors.grey[600],
            style: TextStyle(fontSize: 19),
            decoration: InputDecoration(
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(11.0)),
                borderSide: BorderSide(color: Colors.white, width: 2.0),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(11.0)),
                borderSide: BorderSide(color: Colors.white, width: 2.0),
              ),
              fillColor: Colors.grey[200],
              filled: true,
              hintText: " Search...",
              hintStyle: TextStyle(fontSize: 15, color: Colors.grey[600]),
              suffixIcon: Icon(Icons.search, color: Colors.grey[600]),
              contentPadding:
                  new EdgeInsets.symmetric(vertical: 4.0, horizontal: 10.0),
            ),
          ),
        ),
      ),
      body: Row(
        children: [
          Expanded(
            child: Container(
              height: 820,
              width: 180,
              color: Colors.white,
              child: ListView.builder(
                itemCount: Categories.length,
                itemBuilder: (BuildContext context, int index) => Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          _hasbeenPressed = index;
                        });
                      },
                      child: Container(
                          padding: EdgeInsets.only(left: 20),
                          alignment: Alignment.centerLeft,
                          height: 50,
                          width: 150,
                          color: _hasbeenPressed == index
                              ? Colors.grey[200]
                              : Colors.white,
                          child: Text(Categories[index].name,
                              style: TextStyle(
                                  color: _hasbeenPressed == index
                                      ? Colors.black
                                      : Colors.grey))),
                    ),
                    Divider(height: 0.3, color: Colors.grey[400]),
                  ],
                ),
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: Container(
              margin: EdgeInsets.only(right: 20, left: 35, bottom: 100),
              height: 600,
              child: GridView.builder(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    mainAxisSpacing: 20,
                    crossAxisSpacing: 25,
                    childAspectRatio: 1.2),
                itemCount: Categories[_hasbeenPressed]
                    .categorydetail
                    .length, // need index with list of categories
                itemBuilder: (BuildContext context, int i) => GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => CategoriesPage(
                                  model: Categories[
                                      _hasbeenPressed], // dont need this model if no need to send any info to the next page
                                )));
                  },
                  child: Container(
                    width: 100,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(17.0))),
                    child: Column(
                      children: [
                        Center(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Image.asset(
                                Categories[_hasbeenPressed].categorydetail[i]
                                    ["image"],
                                height: 40,
                                width: 100),
                          ),
                        ),
                        Center(
                            child: Padding(
                          padding: const EdgeInsets.only(bottom: 2.0, top: 2.0),
                          child: Text(
                              Categories[_hasbeenPressed].categorydetail[i]
                                  ["title"],
                              style: TextStyle(fontSize: 13)),
                        )),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
