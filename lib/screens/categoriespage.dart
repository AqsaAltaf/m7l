import 'package:flutter/material.dart';
import 'package:m7l/models/CategoriesImagesList.dart';
import 'package:m7l/models/CategoriesList.dart';
import 'package:m7l/models/CategoryDetailsList.dart';
import 'package:m7l/screens/categoriesmenu.dart';
import 'package:m7l/screens/productpage.dart';
import 'package:m7l/screens/profilepage.dart';
import '../main.dart';
import 'mycartpage.dart';

class CategoriesPage extends StatefulWidget {
  final CategoryDetailsList model;
  CategoriesPage({this.model});
  @override
  _CategoriesPageState createState() => _CategoriesPageState();
}

class _CategoriesPageState extends State<CategoriesPage> {
  int _selectedIndex = 0;
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        title: Text("Laptops",
            style: TextStyle(color: Colors.black, fontSize: 18)),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.black),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => CategoriesMenuPage()),
            );
          },
        ),
      ),
      body: Column(
        children: [
          Container(
            height: 40,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: CategoriesListModel.getCategoriesList().length,
              itemBuilder: (BuildContext context, int index) => Column(
                children: [
                  Container(
                    height: 22,
                    width: 110,
                    margin: EdgeInsets.all(7.0),
                    padding: EdgeInsets.only(left: 1.0),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(16.0))),
                    child: Padding(
                      padding: const EdgeInsets.only(top: 2),
                      child: Text(
                          CategoriesListModel.getCategoriesList()[index].name,
                          textAlign: TextAlign.center),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20.0, bottom: 50),
            child: Container(
              margin: EdgeInsets.all(0),
              height: 600,
              child: GridView.builder(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2),
                itemCount: getCategoriesImagesList().length,
                itemBuilder: (BuildContext context, int index) =>
                    GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ProductPage(
                                  model: getCategoriesImagesList()[index],
                                )));
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Container(
                      width: 170,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius:
                              BorderRadius.all(Radius.circular(20.0))),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Center(
                            child: Image.asset(
                                getCategoriesImagesList()[index].image,
                                height: 100,
                                width: 126),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(bottom: 15),
                            child: Center(
                              child: Text(
                                  getCategoriesImagesList()[index].title,
                                  style: TextStyle(fontSize: 13)),
                            ),
                          ),
                          Align(
                              alignment: Alignment.centerRight,
                              child: Padding(
                                padding: const EdgeInsets.only(bottom: 4.0),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: RichText(
                                    text: TextSpan(
                                      text: 'AED ',
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 10),
                                      children: <TextSpan>[
                                        TextSpan(
                                            text:
                                                getCategoriesImagesList()[index]
                                                    .price,
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 13)),
                                      ],
                                    ),
                                  ),
                                ),
                              )),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
          // ),
          //),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Image.asset('images/Home01.png', height: 30),
            activeIcon: GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MyHomePage()),
                );
              },
              child: Image.asset('images/Home02.png', height: 30),
            ),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.search),
            label: 'Categories',
          ),
          BottomNavigationBarItem(
            icon: Image.asset('images/Shop01.png', height: 30),
            activeIcon: Image.asset('images/Shop02.png', height: 30),
            label: 'Shops',
          ),
          BottomNavigationBarItem(
            icon: GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MyCartPage()),
                );
              },
              child: Icon(Icons.add_shopping_cart),
            ),
            label: 'My Cart',
          ),
          BottomNavigationBarItem(
            icon: Image.asset('images/Profile01.png', height: 30),
            activeIcon: GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ProfilePage()),
                );
              },
              child: Image.asset('images/Profile02.png', height: 30),
            ),
            label: 'Profile',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.black,
        onTap: _onItemTapped,
      ),
    );
  }
}
