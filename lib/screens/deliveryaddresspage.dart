import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:m7l/models/DeliveryAddress.dart';
import 'package:m7l/screens/mycartpage.dart';
import 'package:m7l/screens/newdeliveryaddresspage.dart';
import 'package:m7l/screens/placeorderpage.dart';
import 'package:m7l/userdb.dart';

enum SingingCharacter { selected1, selected2 }

class DeliveryAddressPage extends StatefulWidget {
  DeliveryAddressPage({Key key}) : super(key: key);

  @override
  _DeliveryAddressPageState createState() => _DeliveryAddressPageState();
}

class _DeliveryAddressPageState extends State<DeliveryAddressPage> {
  SingingCharacter _character = SingingCharacter.selected1;
  var box = Hive.box<User>('AddressBox');
  List<AddressDetailModel> addressdetail = [];

  @override
  void initState() {
    getAddress();
    super.initState();
  }

// need another function for hive box because its an async function
  void getAddress() {
    for (int i = 0; i < box.length; i++) {
      addressdetail.add(AddressDetailModel(
          country: box.getAt(i).country,
          city: box.getAt(i).city,
          address: box.getAt(i).address,
          contactnumber: box.getAt(i).contactnumber));
    }
  }

  void deleteAddress(int index) async {
    addressdetail.removeAt(index);
    await box.deleteAt(index);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        title: Text("Select Delivery Address",
            style: TextStyle(color: Colors.black, fontSize: 16)),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.black),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => MyCartPage()),
            );
          },
        ),
      ),
      body: Column(
        children: [
          Expanded(
            child: ListView.builder(
              //itemCount: Address.length,
              itemCount: addressdetail.length,
              itemBuilder: (context, index) => Stack(
                children: [
                  Container(
                    margin: EdgeInsets.all(20),
                    padding: EdgeInsets.only(top: 19, bottom: 10, right: 10),
                    color: Colors.white,
                    child: Row(
                      children: [
                        Transform.scale(
                          scale: 0.9,
                          child: Radio(
                            activeColor: Colors.black,
                            value: SingingCharacter.selected1,
                            groupValue: _character,
                            onChanged: (SingingCharacter value) {
                              setState(() {
                                _character = value;
                              });
                            },
                          ),
                        ),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  Expanded(child: Text('Country')),
                                  Expanded(
                                      //child: Text(Address[index]["Country"])),
                                      child:
                                          Text(addressdetail[index].country)),
                                ],
                              ),
                              Row(
                                children: [
                                  Expanded(child: Text('City')),
                                  Expanded(
                                      //child: Text(Address[index]["City"])),
                                      child: Text(addressdetail[index].city)),
                                ],
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Expanded(child: Text('Address')),
                                  Expanded(
                                      child: Text(
                                    addressdetail[index].address,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                  )),
                                ],
                              ),
                              Row(
                                children: [
                                  Expanded(child: Text('Contact Number')),
                                  Expanded(
                                      child: Text(
                                    addressdetail[index].contactnumber,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                  )),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  // delete icon in container
                  Padding(
                    padding: const EdgeInsets.only(left: 380.0, top: 25),
                    child: GestureDetector(
                        onTap: () {
                          deleteAddress(index);
                        },
                        child:
                            Icon(Icons.delete, size: 23, color: Colors.black)),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(2.0),
            child: Container(
              margin: EdgeInsets.all(20),
              width: 400,
              height: 40,
              color: Colors.white,
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 20.0),
                    child: Text('Add New Shipping Address'),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 130),
                    child: IconButton(
                      icon: Icon(Icons.add_outlined,
                          size: 25, color: Colors.black),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => NewDeliveryAddressPage()),
                        ).then((value) => setState(() {
                              getAddress();
                            }));
                        // then(value) means waiting for future value then we setstate
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
          // Continue Container
          GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => PlaceOrderPage()),
              );
            },
            child: Container(
              height: 90,
              color: Colors.black,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('CONTINUE',
                      style: TextStyle(color: Colors.white, fontSize: 17)),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
  // }
}
