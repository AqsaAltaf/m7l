import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:m7l/models/CategoriesModel.dart';
import 'package:m7l/models/ProductList.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _selectedIndex = 0;
  int _currentIndex = 0;
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  List cardList = [Item1(), Item2(), Item4()];
  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }
    return result;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        leading: Padding(
          padding: const EdgeInsets.only(left: 10.0),
          child: Image.asset('images/APP_ICON_01.png'),
        ),
        automaticallyImplyLeading: false,
        titleSpacing: 0.1,
        leadingWidth: 70,
        title: Container(
          height: 50,
          width: 330,
          margin: EdgeInsets.only(left: 1, bottom: 1, right: 10),
          padding: EdgeInsets.all(10.0),
          child: TextField(
            cursorColor: Colors.grey[600],
            style: TextStyle(fontSize: 19),
            decoration: InputDecoration(
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(11.0)),
                borderSide: BorderSide(color: Colors.white, width: 2.0),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(11.0)),
                borderSide: BorderSide(color: Colors.white, width: 2.0),
              ),
              fillColor: Colors.grey[200],
              filled: true,
              hintText: " Search...",
              hintStyle: TextStyle(fontSize: 15, color: Colors.grey[600]),
              suffixIcon: Icon(Icons.search, color: Colors.grey[600]),
              contentPadding:
                  new EdgeInsets.symmetric(vertical: 4.0, horizontal: 10.0),
            ),
          ),
        ),
      ),
      body: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 6.0, left: 10, right: 10),
            child: CarouselSlider(
              options: CarouselOptions(
                height: 160.0,
                viewportFraction: 1.6,
                //autoPlay: true,
                autoPlayInterval: Duration(seconds: 3),
                autoPlayAnimationDuration: Duration(milliseconds: 800),
                autoPlayCurve: Curves.fastOutSlowIn,
                pauseAutoPlayOnTouch: true,
                aspectRatio: 2.0,
                onPageChanged: (index, reason) {
                  setState(() {
                    _currentIndex = index;
                  });
                },
              ),
              items: cardList.map((card) {
                return Builder(builder: (BuildContext context) {
                  return Container(
                    padding: EdgeInsets.all(3),
                    height: MediaQuery.of(context).size.height * 0.30,
                    width: MediaQuery.of(context).size.width,
                    child: Card(
                      //color: Colors.blue,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25.0),
                      ),
                      child: card,
                    ),
                  );
                });
              }).toList(),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: map<Widget>(cardList, (index, url) {
              return Container(
                width: _currentIndex == index ? 6 : 5,
                height: _currentIndex == index ? 6 : 5,
                margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: _currentIndex == index ? Colors.grey : Colors.grey,
                ),
              );
            }),
          ),
          Container(
              height: 130,
              child: ListView.builder(
                scrollDirection: Axis.horizontal, // to make
                itemCount: CategoriesModel.getCategories().length,
                itemBuilder: (BuildContext context, int index) => Column(
                  children: [
          Padding(
          padding: const EdgeInsets.only(left: 15.0),
          child: CircleAvatar(
            radius: 42,
            backgroundColor: Colors.white,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Image.asset(
                  CategoriesModel.getCategories()[index].image),
            ),
          ),
          ),
          Padding(
          padding: const EdgeInsets.only(left: 15.0),
          child: Text(CategoriesModel.getCategories()[index].name),
          ),
                  ],
                ),
              ),
              ),
         
          Expanded(
            child: ListView.builder(
              scrollDirection:
                  Axis.vertical, // is used to make list view horizontal
              itemCount: getDetails().length,
              itemBuilder: (BuildContext context, int index) => Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  if (index % 2 != 0) ...{
                    Container(
          margin: EdgeInsets.only(
              top: 20, bottom: 20, left: 10, right: 10),
          height: 80,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(20.0)),
            image: DecorationImage(
              image: AssetImage('images/sale.jpg'),
              fit: BoxFit.cover,
            ),
          ),
                    ),
                  },
                  Padding(
                    padding: const EdgeInsets.only(
            left: 20,
            bottom: 10,
            top: 10), // this is for special products
                    child: Text(getDetails()[index].name),
                  ),
                  Container(
                    height: 145,
                    width: 10,
                    child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: getDetails()[index].productdetail.length,
          itemBuilder: (BuildContext context, int i) => Padding(
            padding: const EdgeInsets.only(left: 15),
            child: Container(
              width: 125,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius:
                      BorderRadius.all(Radius.circular(20.0))),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Image.asset(
                      getDetails()[index].productdetail[i].image,
                      height: 100,
                      width: 126),
                  Padding(
                    padding:
                        const EdgeInsets.only(bottom: 4.0, left: 5),
                    child: Text(
                        getDetails()[index].productdetail[i].title,
                        style: TextStyle(fontSize: 11)),
                  ),
                  Align(
                      alignment: Alignment.centerRight,
                      child: Padding(
                        padding: const EdgeInsets.only(
                            bottom: 4.0, right: 5),
                        child: RichText(
                          text: TextSpan(
                            text: 'AED ',
                            style: TextStyle(
                                color: Colors.black, fontSize: 10),
                            children: <TextSpan>[
                              TextSpan(
                                  text: getDetails()[index]
                                      .productdetail[i]
                                      .price,
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 12)),
                            ],
                          ),
                        ),
                      )),
                ],
              ),
            ),
          ),
                    ),
                  ),
                ],
              ),
            ),
          ),
         // ),
        ],
      ),
    );
  }
}

class Item1 extends StatelessWidget {
  const Item1({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(18.0),
        image: DecorationImage(
          image: AssetImage('images/sale.jpg'),
          fit: BoxFit.cover,
        ),
        gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            stops: [
              0.3,
              1
            ],
            colors: [
              Color(0xffff4000),
              Color(0xffffcc66),
            ]),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
      ),
    );
  }
}

class Item2 extends StatelessWidget {
  const Item2({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(18.0),
        image: DecorationImage(
          image: AssetImage('images/sale.jpg'),
          fit: BoxFit.cover,
        ),
        gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            stops: [0.3, 1],
            colors: [Color(0xff5f2c82), Color(0xff49a09d)]),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
      ),
    );
  }
}

class Item4 extends StatelessWidget {
  const Item4({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(18.0),
        image: DecorationImage(
          image: AssetImage('images/sale.jpg'),
          fit: BoxFit.cover,
        ),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
      ),
    );
  }
}
