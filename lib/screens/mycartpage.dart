import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:m7l/cartdb.dart';
import 'package:m7l/main.dart';
import 'package:m7l/models/CartList.dart';
import 'package:m7l/screens/deliveryaddresspage.dart';
import 'package:m7l/screens/profilepage.dart';

//import 'package:google_fonts/google_fonts.dart';

class MyCartPage extends StatefulWidget {
  @override
  _MyCartPageState createState() => _MyCartPageState();
}

class _MyCartPageState extends State<MyCartPage> {
  int _selectedIndex = 0;
  
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  var cartbox = Hive.box<Cart>('CartBox');
  List<CartListModel> cartdetail = [];
  List<int> itemnumber = [];

  @override
  void initState() {
    getitem();
    additem();
    super.initState();
  }
    
  void additem(){
    itemnumber=cartdetail.map((e) => e.quantity).toList();
    print(itemnumber);
  }


// need another function for hive box because its an async function

  void getitem() {
    // print(cartbox.values.length);
    //print(itemnumber.length);
    for (int i = 0; i < cartbox.length; i++) {
      cartdetail.add(CartListModel(
      image: cartbox.getAt(i).image,
      title: cartbox.getAt(i).title,
      price: cartbox.getAt(i).price,
      quantity: cartbox.getAt(i).quantity
      ));
    }
  }

  void addboxitem(){

  }

   void deleteitem(int index) async {
    //print(cartbox.values.length);
    await cartbox.deleteAt(index);
    cartdetail.removeAt(index);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    print('item');
    print(itemnumber.length);
    //print(itemnumber);
    //getitem();
    //print(getCartList().length);
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        title: Text("My Bag", style: TextStyle(color: Colors.black)),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.black),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => MyHomePage()),
            );
          },
        ),
      ),
      body: Column(
        children: [
          Expanded(
            child: ListView.builder(
              //itemCount: getCartList().length,
              itemCount: cartdetail.length,
              itemBuilder: (BuildContext context, int index) => Column(
                children: [
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 18.0, top: 20, right: 14),
                    child: Stack(
                      children: [
                        Container(
                          height: 120,
                          width: 400,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0))),
                          child: Row(
                            children: [
                              // Image.asset(getCartList()[index].image),
                              Image.asset(cartdetail[index].image,
                                  height: 120, width: 120),
                              Padding(
                                padding:
                                    const EdgeInsets.only(left: 7, top: 30),
                                child: Column(
                                  children: [
                                    //Text(getCartList()[index].title),
                                    Text(cartdetail[index].title),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          right: 22, top: 17),
                                      child: RichText(
                                        text: TextSpan(
                                          text: 'AED ',
                                          style: TextStyle(
                                              color: Colors.black, fontSize: 9),
                                          children: <TextSpan>[
                                            TextSpan(
                                                //text:getCartList()[index].price,
                                                text: cartdetail[index].price,
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 12)),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 90.0),
                                child: Row(
                                                            children: [
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: GestureDetector(
                                        onTap: () 
                                        //async
                                        {
                                           setState(() {
                                              itemnumber[index]--;  
                                              print(itemnumber[index]);
                                            });
                                           /* await cartbox.add(Cart(
                                                 //cartdetail[index].image,
                                                 //cartdetail[index].title,
                                                 cartdetail[index].price,
                                                 itemnumber[index],
                                               ));*/
                                        },
                                        child: CircleAvatar(
                                          radius: 7,
                                          backgroundColor: Colors.grey[350],
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                                left: 1.0),
                                            child: Icon(Icons.remove,
                                                size: 11, color: Colors.black),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                             Text(itemnumber[index].toString()),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: GestureDetector(
                                  onTap: () {
                                  setState(() {
                                    itemnumber[index]++;
                                    print(itemnumber[index]);
                                  });
                                  },
                                  child: CircleAvatar(
                                    radius: 7,
                                    backgroundColor: Colors.grey[350],
                                    child: Padding(
                                      padding: const EdgeInsets.only(left: 1.0),
                                      child: Icon(Icons.add,
                                          size: 11, color: Colors.black),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 375, top: 5),
                          child: GestureDetector(
                            onTap: () {
                              deleteitem(index);
                            },
                            child: CircleAvatar(
                              radius: 7,
                              backgroundColor: Colors.red,
                              child: Padding(
                                padding: const EdgeInsets.only(left: 1.0),
                                child: Icon(Icons.close,
                                    size: 11, color: Colors.white),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            height: 90,
            color: Colors.black,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text('Proceed to Checkout |',
                    style: TextStyle(color: Colors.white, fontSize: 17)),
                RichText(
                  text: TextSpan(
                    text: 'AED  ',
                    style: TextStyle(color: Colors.white, fontSize: 14),
                    children: <TextSpan>[
                      TextSpan(text: '8950', style: TextStyle(fontSize: 17)),
                    ],
                  ),
                ),
                IconButton(
                  icon: Icon(Icons.arrow_forward_ios),
                  color: Colors.white,
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => DeliveryAddressPage()),
                    );
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
