import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:m7l/models/DeliveryAddress.dart';
import 'package:m7l/screens/deliveryaddresspage.dart';
import 'package:flutter/services.dart';
import 'package:m7l/userdb.dart';

class NewDeliveryAddressPage extends StatefulWidget {
  @override
  _NewDeliveryAddressPageState createState() => _NewDeliveryAddressPageState();
}

class _NewDeliveryAddressPageState extends State<NewDeliveryAddressPage> {
  final formKey = GlobalKey<FormState>();

  final countrycontroller = TextEditingController();
  final citycontroller = TextEditingController();
  final addresscontroller = TextEditingController();
  final contactnumbercontroller = TextEditingController();

  var box = Hive.box<User>('AddressBox');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        title: Text("Select Delivery Address",
            style: TextStyle(color: Colors.black, fontSize: 16)),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.black),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => DeliveryAddressPage()),
            );
          },
        ),
      ),
      body: Container(
        margin: EdgeInsets.all(20.0),
        child: Form(
          key: formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('Country'),
              ),
              TextFormField(
                keyboardType: TextInputType.text,
                controller: countrycontroller,
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter country';
                  }
                  return null;
                },
                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    borderSide: BorderSide(color: Colors.white, width: 1.0),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    borderSide: BorderSide(color: Colors.white, width: 1.0),
                  ),
                  fillColor: Colors.white,
                  filled: true,
                  isDense: true, // Added this
                  contentPadding: EdgeInsets.all(13),
                ),
              ),
              Divider(color: Colors.grey[200]),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('City'),
              ),
              TextFormField(
                keyboardType: TextInputType.text,
                controller: citycontroller,
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter city';
                  }
                  return null;
                },
                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    borderSide: BorderSide(color: Colors.white, width: 1.0),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    borderSide: BorderSide(color: Colors.white, width: 1.0),
                  ),
                  fillColor: Colors.white,
                  filled: true,
                  isDense: true, // Added this
                  contentPadding: EdgeInsets.all(13),
                ),
              ),
              Divider(color: Colors.grey[200]),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('Address ( Apartment / villa name & number )'),
              ),
              TextFormField(
                keyboardType: TextInputType.streetAddress,
                controller: addresscontroller,
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter address';
                  }
                  return null;
                },
                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    borderSide: BorderSide(color: Colors.white, width: 1.0),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    borderSide: BorderSide(color: Colors.white, width: 1.0),
                  ),
                  isDense: true, // Added this
                  contentPadding: EdgeInsets.all(13),
                  fillColor: Colors.white,
                  filled: true,
                ),
              ),
              Divider(color: Colors.grey[200]),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('Contact Number'),
              ),
              Flexible(
                child: TextFormField(
                  controller: contactnumbercontroller,
                  keyboardType: TextInputType.phone,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter contact number';
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      borderSide: BorderSide(color: Colors.white, width: 1.0),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      borderSide: BorderSide(color: Colors.white, width: 1.0),
                    ),
                    isDense: true, // Added this
                    contentPadding: EdgeInsets.all(13),
                    fillColor: Colors.white,
                    filled: true,
                  ),
                ),
              ),
              Spacer(),
              Padding(
                padding: const EdgeInsets.only(bottom: 60.0),
                child: Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        width: 380,
                        child: RaisedButton(
                          onPressed: () async {
                            /* if (formKey.currentState.validate()) {
                              print('The form is valid');
                              Address.add({
                                "Country": countrycontroller.text,
                                "City": citycontroller.text,
                                "Address": addresscontroller.text,
                                "ContactNumber": contactnumbercontroller.text,
                              });
                            } else {
                              print("The form in invalid");
                            }*/
                            if (formKey.currentState.validate()) {
                              print('The form is valid');
                              await box.add(User(
                                  // storing data in the database
                                  countrycontroller.text,
                                  citycontroller.text,
                                  addresscontroller.text,
                                  contactnumbercontroller.text));
                              Navigator.pop(context);
                            } else {
                              print("The form in invalid");
                            }
                          },
                          color: Colors.black,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            side: BorderSide(),
                          ),
                          child: const Text('SAVE ADDRESS',
                              style:
                                  TextStyle(fontSize: 16, color: Colors.white)),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
