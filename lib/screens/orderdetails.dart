import 'package:flutter/material.dart';
import 'package:m7l/models/OrdersList.dart';

class OrderDetailsPage extends StatefulWidget {
  final OrdersListModel model;
  OrderDetailsPage({this.model});
  @override
  _OrderDetailsPageState createState() => _OrderDetailsPageState();
}

class _OrderDetailsPageState extends State<OrderDetailsPage> {
  bool selected = true;
  bool selected1 = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        backgroundColor: Colors.grey[200],
        elevation: 0.0,
        leading: Stack(
          children: [
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: CircleAvatar(
                radius: 18,
                backgroundColor: Colors.black,
              ),
            ),
            Center(
              child: IconButton(
                icon: Icon(Icons.arrow_back, color: Colors.white, size: 30),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ),
          ],
        ),
      ),
      body: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Padding(
          padding: const EdgeInsets.only(left: 25.0, top: 13),
          child: Row(
            children: [
              Text('Order ', style: TextStyle(fontSize: 16)),
              Text(widget.model.ordernumber, style: TextStyle(fontSize: 16)),
              Padding(
                padding: const EdgeInsets.only(left: 180),
                child: Text('AED ',
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
              ),
              Text(widget.model.price,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 25.0),
          child: Row(
            children: [
              Text('Placed on: ',
                  style: TextStyle(color: Colors.grey[500], fontSize: 13)),
              Text(widget.model.date,
                  style: TextStyle(color: Colors.grey[500], fontSize: 13)),
            ],
          ),
        ),
        Divider(color: Colors.grey[200]),
        Padding(
          padding: const EdgeInsets.only(left: 25.0, top: 20, bottom: 20),
          child: Text('ORDER STATUS', style: TextStyle(fontSize: 15)),
        ),
        ListTile(
          leading: Padding(
            padding: const EdgeInsets.only(top: 15.0),
            child: Image.asset('images/checkmark.png', height: 35, width: 40),
          ),
          title: Padding(
            padding: const EdgeInsets.only(right: 55.0),
            child: Column(
              children: [
                Icon(Icons.shopping_cart, color: Colors.black, size: 30),
                Text('Order has been placed',
                    style: TextStyle(fontSize: 14, color: Colors.black)),
              ],
            ),
          ),
        ),
        Divider(
          indent: 20,
          endIndent: 20,
        ),
        ListTile(
          title: Column(
            children: [
              Icon(Icons.shopping_cart, color: Colors.grey[400], size: 30),
              Text('Preparing your order',
                  style: TextStyle(fontSize: 14, color: Colors.grey[400])),
            ],
          ),
        ),
        Divider(
          indent: 20,
          endIndent: 20,
        ),
        ListTile(
          title: Column(
            children: [
              Icon(Icons.shopping_cart, color: Colors.grey[400], size: 30),
              Text('Preparing your order',
                  style: TextStyle(fontSize: 14, color: Colors.grey[400])),
            ],
          ),
        ),
        Divider(color: Colors.grey[200]),
        Padding(
          padding: const EdgeInsets.all(25.0),
          child: Text('ORDER ITEMS', style: TextStyle(fontSize: 15)),
        ),
        Expanded(
          child: ListView.builder(
            scrollDirection:
                Axis.vertical, // is used to make list view horizontal
            itemCount: getOrdersList().length,
            itemBuilder: (BuildContext context, int index) => Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  height: 130,
                  child: ListView.builder(
                    itemCount: getOrdersList()[index].orderdetail.length,
                    itemBuilder: (BuildContext context, int i) => Column(
                      children: [
                        Container(
                          height: 120,
                          //width: 600,
                          color: Colors.white38,
                          child: Padding(
                            padding: const EdgeInsets.only(
                                top: 4, bottom: 4, right: 60),
                            child: Row(
                              children: [
                                Image.asset(
                                    getOrdersList()[index].orderdetail[i].image,
                                    height: 120,
                                    width: 150),
                                Expanded(
                                  child: Padding(
                                    padding:
                                        const EdgeInsets.only(left: 0, top: 10),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        RichText(
                                          text: TextSpan(
                                            text: 'AED ',
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 14,
                                                fontWeight: FontWeight.bold),
                                            children: <TextSpan>[
                                              TextSpan(
                                                  text: getOrdersList()[index]
                                                      .orderdetail[i]
                                                      .price,
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 14)),
                                            ],
                                          ),
                                        ),
                                        Text(
                                            getOrdersList()[index]
                                                .orderdetail[i]
                                                .title,
                                            style: TextStyle(
                                                color: Colors.grey[500],
                                                fontSize: 12)),
                                        Divider(color: Colors.white),
                                        Row(
                                          children: [
                                            Text('Qty ',
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 14)),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 8.0),
                                              child: Text(': ',
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 14)),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 8.0),
                                              child: Text(
                                                  getOrdersList()[index]
                                                      .orderdetail[i]
                                                      .quantity,
                                                  style:
                                                      TextStyle(fontSize: 14)),
                                            ),
                                          ],
                                        ),

                                        /*RichText(
                              text: TextSpan(
                                text: 'Qty:',
                                style: TextStyle(
                                    color: Colors.black, fontSize: 14, fontWeight: FontWeight.bold),
                                children: <TextSpan>[
                                  TextSpan(
                                      text: getOrdersList()[index]
                                          .orderdetail[i]
                                          .quantity,
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 14)),
                                ],
                              ),
                            ),*/
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                    //},
                  ),
                ),
              ],
            ),
          ),
        ),
      ]),
    );
  }
}
