import 'package:flutter/material.dart';
import 'package:m7l/models/OrdersList.dart';
import 'package:m7l/screens/orderdetails.dart';
import 'package:m7l/screens/profilepage.dart';
import '../main.dart';

class MyOrdersPage extends StatefulWidget {
  @override
  _MyOrdersPageState createState() => _MyOrdersPageState();
}

class _MyOrdersPageState extends State<MyOrdersPage> {
  int _selectedIndex = 0;
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  final statuscontroller = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        title: Text("My Orders",
            style: TextStyle(color: Colors.black, fontSize: 14)),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.black),
          onPressed: () {
            Navigator.pop(
              context,
            );
          },
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 30.0),
        child: ListView.builder(
          scrollDirection: Axis.vertical,
          itemCount: getOrdersList().length,
          itemBuilder: (BuildContext context, int index) => GestureDetector(
        onTap: () {
        Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => OrderDetailsPage(
          model: getOrdersList()[index],
        )));
        },
        child: Padding(
        padding: const EdgeInsets.only(left: 10),
        child: Container(
        margin: EdgeInsets.all(8),
        //width: 400,
        //height: 50,
        decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(12.0))),
        child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
        Padding(
          padding: const EdgeInsets.all(4.0),
          child: Row(
        children: [
        Text('Order '),
        Text(getOrdersList()[index].ordernumber),
        Expanded(
                  child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 180),
                child: RichText(
                  text: TextSpan(
                    text: 'AED ',
                    style: TextStyle(
                        color: Colors.black, fontSize: 15),
                    children: <TextSpan>[
                      TextSpan(
                          text:
                              getOrdersList()[index].price,
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 18)),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(4.0),
          child: Row(
        children: [
        Text('Placed on:',  style: TextStyle(color: Colors.grey[500], fontSize: 13)),
        Text(getOrdersList()[index].date, style: TextStyle(color: Colors.grey[500], fontSize: 12)),
        ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
        children: [
        Text('Status:'),
        Text(getOrdersList()[index].status,
            style: TextStyle(
                color: getOrdersList()[index].status ==
                        'Delivered'
                    ? Colors.green
                    : Colors.yellow[800])),
        Expanded(
                child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Image.asset(getOrdersList()[index].image,
                      height: 40, width: 80),
                  Text('Track Order'),
                ],
              ),
        ),
        ],
          ),
        ),
        ],
        ),
        ),
        ),
          ),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Image.asset('images/Home01.png', height: 30),
            activeIcon: GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MyHomePage()),
                );
              },
              child: Image.asset('images/Home02.png', height: 30),
            ),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.search),
            label: 'Categories',
          ),
          BottomNavigationBarItem(
            icon: Image.asset('images/Shop01.png', height: 30),
            activeIcon: Image.asset('images/Shop02.png', height: 30),
            label: 'Shops',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.add_shopping_cart),
            label: 'My Cart',
          ),
          BottomNavigationBarItem(
            icon: Image.asset('images/Profile01.png', height: 30),
            activeIcon: GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ProfilePage()),
                );
              },
              child: Image.asset('images/Profile02.png', height: 30),
            ),
            label: 'Profile',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.black,
        onTap: _onItemTapped,
      ),
    );
  }
}
