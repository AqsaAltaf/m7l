//import 'dart:html';

import 'package:flutter/material.dart';
import 'package:m7l/screens/newdeliveryaddresspage.dart';
import 'package:circular_check_box/circular_check_box.dart';

class PlaceOrderPage extends StatefulWidget {
  @override
  _PlaceOrderPageState createState() => _PlaceOrderPageState();
}

class _PlaceOrderPageState extends State<PlaceOrderPage> {
  bool selected = true;
  bool selected1 = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        title: Text("Select Delivery Address",
            style: TextStyle(color: Colors.black, fontSize: 16)),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.black),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => NewDeliveryAddressPage()),
            );
          },
        ),
      ),
      body: Column(
        //crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.all(18),
            padding: EdgeInsets.all(15),
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 5,
                    blurRadius: 7,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ],
                /*  boxShadow: [
                  BoxShadow(
                    color: Colors.grey[300],
                    blurRadius: 60.0, // soften the shadow
                    spreadRadius: 10.0, //extend the shadow
                  )
                ],*/
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(14.0))),
            child: Table(
              columnWidths: {0: FractionColumnWidth(.5)},
              children: [
                TableRow(children: [
                  SizedBox(
                      height: 30,
                      child: Text("item(s)",
                          textScaleFactor: 0.9,
                          style: TextStyle(fontWeight: FontWeight.bold))),
                  Padding(
                    padding: const EdgeInsets.only(left: 24.0),
                    child: Text("QTY",
                        textScaleFactor: 0.9,
                        style: TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 30.0),
                    child: Text("Price",
                        textScaleFactor: 0.9,
                        style: TextStyle(fontWeight: FontWeight.bold)),
                  ),
                ]),
                TableRow(children: [
                  SizedBox(
                      height: 30,
                      child: Text("pink women dress", textScaleFactor: 0.9)),
                  Text("2", textScaleFactor: 0.9, textAlign: TextAlign.center),
                  SizedBox(
                      height: 30,
                      child: Text("AED 860",
                          textScaleFactor: 0.9, textAlign: TextAlign.end)),
                ]),
                TableRow(children: [
                  Text("blue women dress", textScaleFactor: 0.9),
                  Text("1", textScaleFactor: 0.9, textAlign: TextAlign.center),
                  SizedBox(
                      height: 30,
                      child: Text("AED 850",
                          textScaleFactor: 0.9, textAlign: TextAlign.end)),
                ]),
                TableRow(children: [
                  Text("", textScaleFactor: 0.9),
                  Text("Subtotal", textScaleFactor: 0.9),
                  SizedBox(
                      height: 30,
                      child: Text("AED 1710",
                          textScaleFactor: 0.9, textAlign: TextAlign.end)),
                ]),
                TableRow(children: [
                  Text("", textScaleFactor: 0.9),
                  Text("Delivery", textScaleFactor: 0.9),
                  SizedBox(
                      height: 30,
                      child: Text("AED 25",
                          textScaleFactor: 0.9, textAlign: TextAlign.end)),
                ]),
                TableRow(children: [
                  Text("", textScaleFactor: 0.9),
                  Text("Coupon Code", textScaleFactor: 0.9),
                  SizedBox(
                      height: 30,
                      child: Text("AED - 30",
                          textScaleFactor: 0.9,
                          textAlign: TextAlign.end,
                          style: TextStyle(color: Colors.red))),
                ]),
                TableRow(children: [
                  Text("", textScaleFactor: 0.9),
                  Text("TOTAL", textScaleFactor: 0.9),
                  SizedBox(
                      height: 30,
                      child: Text("AED 1705",
                          textScaleFactor: 0.9, textAlign: TextAlign.end)),
                ]),
              ],
            ),
          ),

          /*Container(
        
            margin: EdgeInsets.all(20),
            padding: EdgeInsets.all(15),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(14.0))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    RichText(
                        text: TextSpan(
                          text: 'item(s)',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 12,
                              color: Colors.black),
                        ),
                      ),
                    Divider(height: 10),
                     Text('pink women dress'),
                     Divider(height: 10),
                    Text('blue women dress'),
                    Divider(height: 125),
                  ],
                ),
                  Spacer(flex: 2),
                  Column(
                 //   crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                         RichText(
                      text: TextSpan(
                        text: 'QTY',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 12,
                            color: Colors.black),
                      ),
                    ),
                   Divider(height: 10),
                    Text('2'),
                    Divider(height: 10),
                    Text('1'),
                    Divider(height: 20),
                    Text('Subtotal'),
                    Divider(height: 10),
                    Text('Delivery'),
                    Divider(height: 10),
                    Text('Coupon Code'),
                    Divider(height: 10),
                    Text('TOTAL'),
                    Divider(height: 10),
                    ],
                  ),
                    
                    Spacer(),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        RichText(
                      text: TextSpan(
                        text: 'Price',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 12,
                            color: Colors.black),
                      ),
                    ),
                    Divider(height: 10),
                     Text('AED 860'),
                     Divider(height: 10),
                    Text('AED 850'),
                    Divider(height: 20),
                    Text('AED 1710'),
                     Divider(height: 10),
                    Text('AED 25'),
                    Divider(height: 10),
                    Text('AED - 30', style: TextStyle(color: Colors.red)),
                     Divider(height: 10),
                    Text('AED 1705'),
                    Divider(height: 10),
                      ],
                    ),
                    
                      ],
            ),
        
          ),*/
          Container(
            margin: EdgeInsets.only(left: 20, top: 10, right: 20, bottom: 25),
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 5,
                    blurRadius: 7,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ],
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(9.0))),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 13.0),
                        child: TextField(
                          decoration: InputDecoration(
                            labelText: 'MVP10',
                            focusedBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)),
                              borderSide:
                                  BorderSide(color: Colors.white, width: 1.0),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)),
                              borderSide:
                                  BorderSide(color: Colors.white, width: 10.0),
                            ),
                            fillColor: Colors.grey[200],
                            filled: true,
                            isDense: true,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 30,
                      child: Padding(
                        padding: const EdgeInsets.only(right: 20.0),
                        child: RaisedButton(
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => PlaceOrderPage()),
                            );
                          },
                          color: Colors.black,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(3.0),
                            side: BorderSide(),
                          ),
                          child: Text('APPLY',
                              style:
                                  TextStyle(fontSize: 14, color: Colors.white)),
                        ),
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 22, bottom: 4),
                  child: Text(
                      '10% discount for mvp application and game design',
                      style: TextStyle(color: Colors.green, fontSize: 9)),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 220, bottom: 10),
            child: Text('Select Payment Method'),
          ),
          Container(
            padding: EdgeInsets.all(8),
            color: Colors.white,
            child: Column(
              children: <Widget>[
                ListTile(
                  leading: CircularCheckBox(
                      value: this.selected,
                      checkColor: Colors.white,
                      activeColor: Colors.green,
                      //inactiveColor: Colors.redAccent,
                      disabledColor: Colors.grey,
                      onChanged: (val) => this.setState(() {
                            this.selected = !this.selected;
                          })),
                  title:
                      Text('Cash On Delivery', style: TextStyle(fontSize: 13)),
                  trailing: ColorFiltered(
                    child:
                        Image.asset('images/cash.png', height: 40, width: 40),
                    colorFilter:
                        ColorFilter.mode(Colors.grey[300], BlendMode.srcIn),
                  ),
                ),
                ListTile(
                  leading: CircularCheckBox(
                      value: this.selected1,
                      checkColor: Colors.white,
                      activeColor: Colors.green,
                      //inactiveColor: Colors.redAccent,
                      disabledColor: Colors.grey,
                      onChanged: (val) => this.setState(() {
                            this.selected1 = !this.selected1;
                          })),
                  title: Text('Credit / Debit Card',
                      style: TextStyle(fontSize: 13)),
                  trailing: ColorFiltered(
                      colorFilter:
                          ColorFilter.mode(Colors.grey[400], BlendMode.srcIn),
                      child: Image.asset('images/visaandmastercardlogo.png',
                          height: 80, width: 100)),
                ),
              ],
            ),
          ),
          Spacer(),
          Padding(
            padding: const EdgeInsets.only(bottom: 60.0),
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    width: 380,
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => PlaceOrderPage()),
                        );
                      },
                      color: Colors.black,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(9.0),
                        side: BorderSide(),
                      ),
                      child: const Text('PLACE ORDER',
                          style: TextStyle(fontSize: 16, color: Colors.white)),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
