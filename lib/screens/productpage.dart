import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:m7l/cartdb.dart';
import 'package:m7l/models/CategoriesImagesList.dart';
import 'package:m7l/screens/categoriespage.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:m7l/screens/mycartpage.dart';

class ProductPage extends StatefulWidget {
  final CategoriesImagesListModel model;
  ProductPage({this.model});
  @override
  _ProductPageState createState() => _ProductPageState();
}

class _ProductPageState extends State<ProductPage> {
  String _value;
  int _currentIndex = 0;
  int _choosecolor = 0;
  List<String> cardList = [];
  List<Color> colorsList = [];
  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }
    return result;
  }

  @override
  void initState() {
    cardList.add(widget.model.image);
    colorsList.addAll(widget.model.color);
    // _value=widget.model.size.first;
    //getcolor();
    super.initState();
  }

  

  var cartbox = Hive.box<Cart>('CartBox');

  @override
  Widget build(BuildContext context) {
    print(widget.model.size.first);

    var sizedBox = SizedBox(
      width: 20,
      child: RaisedButton(
        onPressed: () {},
        color: Colors.black,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(9.0),
          side: BorderSide(),
        ),
        child: Row(
          children: [
            Icon(Icons.shopping_cart, color: Colors.white),
            Text('Checkout',
                style: TextStyle(fontSize: 14, color: Colors.white)),
          ],
        ),
      ),
    );
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.black),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => CategoriesPage()),
            );
          },
        ),
        title: Image.asset('images/APP_ICON_01.png', height: 60, width: 60),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          CarouselSlider(
            options: CarouselOptions(
              viewportFraction: 1.0,
              height: 300.0,
              //autoPlay: true,
              autoPlayInterval: Duration(seconds: 3),
              autoPlayAnimationDuration: Duration(milliseconds: 800),
              autoPlayCurve: Curves.fastOutSlowIn,
              pauseAutoPlayOnTouch: true,
              aspectRatio: 2.0,
              onPageChanged: (index, reason) {
                setState(() {
                  _currentIndex = index;
                });
              },
            ),
            items: cardList.map((card) {
              return Builder(builder: (BuildContext context) {
                return Container(
                  padding: EdgeInsets.all(3),
                  height: MediaQuery.of(context).size.height * 0.30,
                  width: MediaQuery.of(context).size.width,
                  child: Card(
                    color: Colors.white,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(0),
                    ),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(0),
                        image: DecorationImage(
                          image: AssetImage(card),
                          fit: BoxFit.fill,
                        ),
                        /* gradient: LinearGradient(
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                            stops: [0.3, 1],
                            colors: [Color(0xff5f2c82), Color(0xff49a09d)]),*/
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                      ),
                    ),
                  ),
                );
              });
            }).toList(),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: map<Widget>(cardList, (index, url) {
              return Container(
                width: _currentIndex == index ? 6 : 5,
                height: _currentIndex == index ? 6 : 5,
                margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: _currentIndex == index ? Colors.grey : Colors.grey,
                ),
              );
            }),
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(widget.model.title,
                          style: TextStyle(color: Colors.black, fontSize: 18)),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0, bottom: 8),
                      child: Text(widget.model.description,
                          style:
                              TextStyle(color: Colors.grey[600], fontSize: 17)),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0, top: 8),
                      child: Text('Size',
                          style: TextStyle(color: Colors.black, fontSize: 16)),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        width: 150,
                        height: 30,
                        color: Colors.white,
                        child: DropdownButton(
                          underline: SizedBox(),
                          hint: Text('Select Size',
                              style: TextStyle(color: Colors.grey[600])),
                          style: new TextStyle(
                            color: Colors.black,
                          ),
                          icon: Padding(
                            padding: const EdgeInsets.only(left: 8.0, top: 3),
                            child: Icon(
                                // Add this
                                Icons.keyboard_arrow_down,
                                color: Colors.black),
                          ),
                          items: widget.model.size.map((e) => DropdownMenuItem(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(e,
                                    style: TextStyle(color: Colors.grey[600])),
                              ),
                              value: e,
                            ),).toList(),
                          onChanged: (value) {
                            setState(() {
                              _value = value;
                            });
                          },
                          value: _value,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text('Color',
                          style: TextStyle(color: Colors.black, fontSize: 16)),
                    ),
                    Container(
                      height: 50,
                      child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        //itemCount: getCategoriesImagesList().length,
                        itemCount: colorsList.length,
                        itemBuilder: (BuildContext context, int index) => Row(
                          children: [
                            Stack(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        _currentIndex = index;
                                        print('selected');
                                      });
                                    },
                                    child: CircleAvatar(
                                      radius: _currentIndex == index ? 16 : 14,
                                      backgroundColor: Colors.green,
                                      child: CircleAvatar(
                                        radius: 14,
                                        backgroundColor: colorsList[index],
                                      ),
                                    ),
                                  ),
                                ),
                                if (_currentIndex == index) ...{
                                  Image.asset('images/checkmark.png',
                                      height: 27),
                                }
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    /*Row(
                      children: [
                        Stack(
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: GestureDetector(
                                onTap: () {
                                  setState(() {
                                    _choosecolor = _currentIndex;
                                    print('selected');
                                  });
                                },
                                child: CircleAvatar(
                                  radius: 19,
                                  backgroundColor: _choosecolor == _currentIndex
                                      ? Colors.blue
                                      : Colors.green,
                                ),
                              ),
                            ),
                            Image.asset('images/checkmark.png', height: 30),
                          ],
                        ),
                        Stack(
                          children: [
                            CircleAvatar(
                              radius: 14,
                              backgroundColor: colorsList[1],
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Stack(
                            children: [
                              CircleAvatar(
                                radius: 14,
                                backgroundColor: colorsList[2],
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),*/
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text('Product Properties',
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 16,
                              fontWeight: FontWeight.bold)),
                    ),
                    Row(
                      children: [
                        Image.asset('images/checkmark.png', height: 30),
                        Expanded(
                          child: Text('Material:',
                              style: TextStyle(
                                  fontSize: 14, color: Colors.grey[500])),
                        ),
                        Expanded(
                            flex: 2,
                            child: Text(widget.model.material,
                                style: TextStyle(fontSize: 14))),
                      ],
                    ),
                    Row(
                      children: [
                        Image.asset('images/checkmark.png', height: 30),
                        Expanded(
                          child: Text('Style Collection:',
                              style: TextStyle(
                                  fontSize: 14, color: Colors.grey[500])),
                        ),
                        Expanded(
                          flex: 2,
                          child: Text(widget.model.stylecollection,
                              style: TextStyle(fontSize: 14)),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: RaisedButton(
                            onPressed: () async {
                              await cartbox.add(Cart(
                                widget.model.image,
                                widget.model.title,
                                widget.model.price,
                                1,
                              ));
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => MyCartPage()));
                            },
                            color: Colors.black,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(9.0),
                              side: BorderSide(),
                            ),
                            child: Row(
                              children: [
                                Icon(Icons.shopping_cart, color: Colors.white),
                                Padding(
                                  padding: const EdgeInsets.all(3.0),
                                  child: Text('Add to Cart',
                                      style: TextStyle(
                                          fontSize: 14, color: Colors.white)),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 70.0),
                          child: RaisedButton(
                            onPressed: () {},
                            color: Colors.black,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(9.0),
                              side: BorderSide(),
                            ),
                            child: Row(
                              children: [
                                Icon(Icons.shopping_cart, color: Colors.white),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text('Checkout',
                                      style: TextStyle(
                                          fontSize: 14, color: Colors.white)),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
