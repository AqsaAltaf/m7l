import 'package:flutter/material.dart';
import 'package:m7l/screens/categoriesmenu.dart';
import 'package:m7l/screens/mycartpage.dart';
import 'package:m7l/screens/orderspage.dart';
import 'package:m7l/screens/shopspage.dart';

import '../main.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  int _selectedIndex = 0;
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        title: Text(
          "Profile",
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: Column(
        children: [
          Stack(
            children: [
              Container(
                height: 400,
                width: 400,
                margin: EdgeInsets.only(top: 60, left: 10, right: 10),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(16.0))),
                child: Padding(
                  padding: const EdgeInsets.only(top: 80.0, left: 18, right: 0),
                  child: ListView(
                    children: <Widget>[
                      ListTile(
                        title:
                            Text('My Orders', style: TextStyle(fontSize: 13)),
                        trailing: IconButton(
                          icon: Icon(Icons.arrow_forward_ios, size: 13),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => MyOrdersPage()),
                            );
                          },
                        ),
                      ),
                      Divider(
                          color: Colors.grey,
                          height: 0,
                          indent: 15,
                          endIndent: 16),
                      ListTile(
                        dense: true,
                        title: Text('My Addresses',
                            style: TextStyle(fontSize: 13)),
                        trailing: IconButton(
                          icon: Icon(Icons.arrow_forward_ios, size: 13),
                          onPressed: () {},
                        ),
                      ),
                      Divider(
                          color: Colors.grey,
                          height: 0.2,
                          indent: 15,
                          endIndent: 16),
                      ListTile(
                        title: Text('Choose Language',
                            style: TextStyle(fontSize: 13)),
                        trailing: IconButton(
                          icon: Icon(Icons.arrow_forward_ios, size: 13),
                          onPressed: () {},
                        ),
                      ),
                      Divider(
                          color: Colors.grey,
                          height: 0.2,
                          indent: 15,
                          endIndent: 16),
                      ListTile(
                        title: Text('Support', style: TextStyle(fontSize: 13)),
                        trailing: IconButton(
                          icon: Icon(Icons.arrow_forward_ios, size: 13),
                          onPressed: () {},
                        ),
                      ),
                      Divider(
                          color: Colors.grey,
                          height: 0.2,
                          indent: 15,
                          endIndent: 16),
                      ListTile(
                        title: Text('Logout', style: TextStyle(fontSize: 13)),
                        trailing: IconButton(
                          icon: Icon(Icons.arrow_forward_ios, size: 13),
                          onPressed: () {},
                        ),
                      ),
                      Divider(
                          color: Colors.grey,
                          height: 0.2,
                          indent: 15,
                          endIndent: 16),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 160, top: 10),
                child: CircleAvatar(
                  radius: 55,
                  backgroundColor: Colors.grey[400],
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ColorFiltered(
                      child: Image.network(
                          "https://i.dlpng.com/static/png/6456172_preview.png"),
                      colorFilter:
                          ColorFilter.mode(Colors.white, BlendMode.srcIn),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 128.0, left: 177),
                child: RichText(
                  text: TextSpan(
                    text: 'Hello, ',
                    style: TextStyle(color: Colors.black, fontSize: 13),
                    children: <TextSpan>[
                      TextSpan(
                          text: 'Rashid',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 12)),
                    ],
                  ),
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(left: 75.0, top: 150),
            child: Row(
              children: [
                Text('Terms & Conditons . ',
                    style: TextStyle(fontSize: 9, color: Colors.grey[700])),
                Text('Privacy Policy . ',
                    style: TextStyle(fontSize: 9, color: Colors.grey[700])),
                Text('Refund Policy . ',
                    style: TextStyle(fontSize: 9, color: Colors.grey[700])),
                Text('Delivery Policy',
                    style: TextStyle(fontSize: 9, color: Colors.grey[700])),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text("Version 1.0.0",
                style: TextStyle(fontSize: 9, color: Colors.grey[700])),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 140),
            child: Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 7.0),
                  child: Image.asset('images/done-by-youth-logo.png',
                      height: 30, width: 30),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0),
                  child: Image.asset('images/visa.png', height: 40, width: 40),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0),
                  child: Image.asset('images/mastercard.png',
                      height: 30, width: 40),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
