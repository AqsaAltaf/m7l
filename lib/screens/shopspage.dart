import 'package:flutter/material.dart';
import 'package:m7l/main.dart';
import 'package:m7l/models/OtherShops.dart';
import 'package:m7l/models/ShopList.dart';
import 'package:m7l/models/VerifiedShops.dart';
import 'package:m7l/screens/categoriesmenu.dart';
import 'package:m7l/screens/mycartpage.dart';
import 'package:m7l/screens/profilepage.dart';

class ShopsPage extends StatefulWidget {
  @override
  _ShopsPageState createState() => _ShopsPageState();
}

class _ShopsPageState extends State<ShopsPage> {
  int index1 = 0;
  int _selectedIndex = 0;
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        automaticallyImplyLeading: false,
        titleSpacing: 0.1,
        leadingWidth: 40,
        title: Container(
          height: 50,
          width: 330,
          margin: EdgeInsets.only(left: 10, bottom: 1, right: 10),
          padding: EdgeInsets.all(10.0),
          child: TextField(
            cursorColor: Colors.grey[600],
            style: TextStyle(fontSize: 19),
            decoration: InputDecoration(
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(11.0)),
                borderSide: BorderSide(color: Colors.white, width: 2.0),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(11.0)),
                borderSide: BorderSide(color: Colors.white, width: 2.0),
              ),
              fillColor: Colors.grey[200],
              filled: true,
              hintText: " Search shop...",
              hintStyle: TextStyle(fontSize: 15, color: Colors.grey[600]),
              suffixIcon: Icon(Icons.search, color: Colors.grey[600]),
              contentPadding:
                  new EdgeInsets.symmetric(vertical: 4.0, horizontal: 10.0),
            ),
          ),
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 25.0, left: 10, bottom: 8),
            child: Text('Featured Shops'),
          ),
          Container(
            height: 105,
            width: 420,
            child: GridView.builder(
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 1,
                  childAspectRatio: 1.10,
                  mainAxisSpacing: 3),
              scrollDirection:
                  Axis.horizontal, // is used to make list view horizontal
              itemCount: getShopDetails()[index1].shopdetail.length,
              itemBuilder: (BuildContext context, int index) => Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 10.0),
                    child: Container(
                      height: 80,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(20.0)),
                        image: DecorationImage(
                          image: AssetImage(
                              getShopDetails()[index1].shopdetail[index].image),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Text(getShopDetails()[index1].shopname[index].title,
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 9)),
                  ),
                ],
              ),
            ),
          ),
          Divider(color: Colors.grey[600], indent: 40, endIndent: 40),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text('Verified Shops'),
          ),
          Container(
            height: 210,
            width: 430,
            child: GridView.builder(
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  childAspectRatio: 1.08,
                  mainAxisSpacing: 3),
              scrollDirection:
                  Axis.horizontal, // is used to make list view horizontal
              itemCount:
                  getVerfiedShopDetails()[index1].verifiedshopdetail.length,
              itemBuilder: (BuildContext context, int index) => Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 10.0),
                    child: Container(
                      width: 500,
                      height: 80,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(20.0)),
                        image: DecorationImage(
                          image: AssetImage(getVerfiedShopDetails()[index1]
                              .verifiedshopdetail[index]
                              .image),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 3.0),
                        child: Image.asset('images/badge.jpg', height: 10),
                      ),
                     Expanded(
                                            child: Align(alignment: Alignment.center,
                                              child: Text(
                              getVerfiedShopDetails()[index1]
                                  .verifiedshopname[index]
                                  .title,
                              style: TextStyle(fontSize: 9)),
                       ),
                     ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Divider(color: Colors.grey[600], indent: 40, endIndent: 40),
          Padding(
            padding: const EdgeInsets.only(left: 10, bottom: 10),
            child: Text('Other Shops'),
          ),
          Expanded(
            child: GridView.builder(
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 4,
                  childAspectRatio: 1.09,
                  crossAxisSpacing: 8,
                  mainAxisSpacing: 9),
              scrollDirection:
                  Axis.vertical, // is used to make list view horizontal
              itemCount: getOtherShopDetails()[index1].othershopdetail.length,
              itemBuilder: (BuildContext context, int index) => Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 10.0),
                    child: Container(
                     
                      height: 80,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(20.0)),
                        image: DecorationImage(
                          image: AssetImage(getOtherShopDetails()[index1]
                              .othershopdetail[index]
                              .image),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Text(
                        getOtherShopDetails()[index1]
                            .othershopname[index]
                            .title,
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 10)),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
