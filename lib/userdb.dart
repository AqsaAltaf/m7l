import 'package:hive/hive.dart';
part 'userdb.g.dart';

@HiveType(typeId: 0) // this is like generating a table
class User extends HiveObject{
  @HiveField(0)
  String country;
  @HiveField(1)
  String city;
  @HiveField(2)
  String address;
  @HiveField(3)
  String contactnumber;
  User(this.country, this.city, this.address, this.contactnumber);
}